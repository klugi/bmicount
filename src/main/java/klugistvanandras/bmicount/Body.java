package klugistvanandras.bmicount;

import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

public class Body {

	private double height;
	private double weight;
	private static Set<String> datatype = new HashSet<>();
	private String dtype;
	
	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public static Set<String> getDatatype() {
		return datatype;
	}

	public static void setDatatype(Set<String> datatype) {
		Body.datatype = datatype;
	}

	public String getDtype() {
		return dtype;
	}

	public void setDtype(String dtype) {
		this.dtype = dtype;
	}

	/**
	 * Body constructor declaration. This function has no return type but has three
	 * parameter.  In bad parameters case the constructor throws IllegalArgumentExceptions. 
	 * 
	 * @param height person height in meters, centimeters or inches
	 * @param weight person weight in kilograms
	 * @param dtype describe what type measurement was added at the height parameter
	 */
	Body(double height,double weight, String dtype){

		
		if(height <= 0){
			throw new IllegalArgumentException("Height can not be 0 or negative.");
		}
		
		if(weight <= 0){
			throw new IllegalArgumentException("Weight can not be 0 or negative.");
		}
		
		datatype.addAll(Arrays.asList("meter", "inches", "centimeter"));
		
		if(!(datatype.contains(dtype))){
			throw new IllegalArgumentException("Illegal number type."); 
		}
		
		
		
		this.height=height;
		this.weight=weight;
		this.dtype=dtype;
	}
	
	
}
