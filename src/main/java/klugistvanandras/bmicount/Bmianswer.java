package klugistvanandras.bmicount;

public class Bmianswer {

	String answer;

	 /**
	  * This method is the Bmi result test unit. Puts The counted bmi result into category
	  * and gives an evaluation.
	  * 
	  * @param bmi counted result
	  * @return    add evaluation for the counted bmi measure
 	  */
	public String bmiAnswer(double bmi) {

		if (bmi < 16) {
			answer = " Severe Thinness";
		} else if (16 < bmi && bmi < 17) {
			answer = " Moderate Thinness";
		} else if (17 < bmi && bmi < 18.5) {
			answer = " Mild Thinness";
		} else if (18.5 < bmi && bmi < 25) {
			answer = " Normal";
		} else if (25 < bmi && bmi < 30) {
			answer = " Overweight";
		} else if (30 < bmi && bmi < 35) {
			answer = " Obese Class I";
		} else if (35 < bmi && bmi < 40) {
			answer = " Obese Class II";
		} else if (bmi > 40) {
			answer = " Obese Class III";
		}

		return answer;
	}

	   
	/**
	 * This method generates the answer evaluation text depends on the body value.
	 * Chooses from three option value based the body dtype value. Invokes the appropriate
	 * bmicount method.
	 * 
	 * @param body person data: weight, height, measurment type 
	 * @return     the counted bmi result and the evaluation message 
	 */
public static String answer(Body body){
		
		Bmicount bmicount = new Bmicount();
		String answerText = "";
		switch(body.getDtype()){
		  
		case "meter" : answerText = bmicount.bmiCountInMeters(body.getHeight(), body.getWeight()); break;
		case "inches": answerText = bmicount.bmiCountInInches(body.getHeight(), body.getWeight()); break;
		case  "centimeter" : answerText = bmicount.bmiCountInCentiMeters(body.getHeight(), body.getWeight()); break;
		
		}
		
		return answerText;
      }
}
