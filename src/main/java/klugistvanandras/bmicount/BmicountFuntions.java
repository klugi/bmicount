package klugistvanandras.bmicount;

public interface BmicountFuntions {

	
	
	public String bmiCountInMeters(double height, double weight);

	
	public String bmiCountInInches(double height, double weight);

	
	public String bmiCountInCentiMeters(double height, double weight);

}
