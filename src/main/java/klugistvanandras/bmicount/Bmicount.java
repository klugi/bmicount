package klugistvanandras.bmicount;

import java.text.NumberFormat;
import java.util.Locale;

public class Bmicount implements BmicountFuntions {

	public double bmi;
	Bmianswer bmianswer = new Bmianswer();

	
	/**
	 * bmiCountInMeters describe the bmicount method in three way 
	 * The first way the parameters are in meter and kilogram no need to
	 * transform them.
	 * The function has two parameters. 
	 * 
	 * @param height the person height in meters
	 * @param weight the person weight in kilogram
	 * @return       the counted bmi result and give an evaluation 
	 */
	
	public String bmiCountInMeters(double height, double weight) {

		NumberFormat nf= NumberFormat.getInstance(new Locale("hu", "Hu"));
		nf.setMaximumFractionDigits(2);
		bmi = weight / Math.pow(height, 2);

		return nf.format(bmi) + bmianswer.bmiAnswer(bmi);
	}

	/**
	 * The second way bmiCountInInches has two parameters height in inches
	 * and weight in kilogram. The inches measurement need to change into meters
	 *   
	 * @param height the person height in Inches
	 * @param weight the person weight in kilogram
	 * @return the counted bmi result and give an evaluation
	 */
	
	public String bmiCountInInches(double height, double weight) {

		NumberFormat nf= NumberFormat.getInstance(new Locale("hu", "Hu"));
		nf.setMaximumFractionDigits(2);
		bmi = weight / Math.pow(height * 0.0254, 2);

		return nf.format(bmi) + bmianswer.bmiAnswer(bmi);
	}

	/**
	 * The third way bmiCountInCentMeters has two parameters height in centimeters
	 * and weight in kilogram. The centimeter measurement need to change into meters
	 *   
	 * @param height the person height in Inches
	 * @param weight the person weight in kilogram
	 * @return the counted bmi result and give an evaluation
	 */
	
	public String bmiCountInCentiMeters(double height, double weight) {

		NumberFormat nf= NumberFormat.getInstance(new Locale("hu", "Hu"));
		nf.setMaximumFractionDigits(2);
		bmi = weight / Math.pow(height / 100, 2);

		return nf.format(bmi) + bmianswer.bmiAnswer(bmi);
	}

}
