package klugistvanandras.bmicount;

public class App {
	public static void main(String[] args) {

		/**
		 * The App is bmi value counter. It counts bmi value from body height,weight data.
		 * The count method depends on what type data was given to the body object.
		 * It counts meters, centimeters and inches. By the counted result the app
		 * gives an evaluation message.
		 * 
		 * 
		 * @author Klug István András
		 * @version 1.0
		 */
		Body body = new Body(1.7, 80, "meter");
		Body body1 = new Body(167, 67, "centimeter");
		Body body2 = new Body(70.86, 89, "inches");

		System.out.println(Bmianswer.answer(body));
		System.out.println(Bmianswer.answer(body1));
		System.out.println(Bmianswer.answer(body2));

	}
}
