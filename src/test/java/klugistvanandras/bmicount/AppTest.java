package klugistvanandras.bmicount;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.Before;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
	private Body body;

	@Before
	public void setUp() throws Exception{
		
		body = new Body(1.7, 80, "meter");
		
	}
	
	
	@Test
	public void testResultWhenParametersPositiv(){
		
	    String result = Bmianswer.answer(body);
		
		assertEquals("27,68 Overweight",result);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testExceptionWhenParameterNegative(){
		Body body1;
		try{
		body1= new Body(-0.6, 80 ,"meter");
		}
		finally{
			
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testExceptionWhenOtherParameterNegative(){
		Body body1;
		try{
		body1= new Body(1.6, -80 ,"meter");
		}
		finally{
			
		}
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void testExceptionWhenParameterNotInSet(){
		Body body1;
		try{
		body1= new Body(1.6, 80 ,"meter_");
		}
		finally{
			
		}
	}
	
}
